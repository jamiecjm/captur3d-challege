json.extract! cardboard, :id, :cardboard_type, :price, :created_at, :updated_at
json.url cardboard_url(cardboard, format: :json)
